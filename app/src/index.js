import "./styles/styleRouterApp/RouterApp.css";
import "./styles/stylePaginaCliente/MisCompras.css";
//importamos la libreria ReactDom para poder mostrar todo el contenido en el elemento con id root

import ReactDom from "react-dom";
import RouterApp from "./router/RouterApp";
//importamos los achivos de los estilos para cada componente de la app
import "./styles/stylePaginaAdmin/paginaAdmin.css";
import  "./styles/stylesPaginaInicio/NavBar.css";
import  "./styles/stylesPaginaInicio/ContenidoInicio.css";
import "./styles/stylesPaginaInicio/Footer.css";
import "./styles/stylesPaginaInicio/Mediaquery.css";
import "./styles/stylesPaginaInicio/Nosotros.css";
import "./styles/stylesPaginaInicio/Producto.css";
import "./styles/styleModal/Modal.css";
import"./styles/styleModal/ModalRegistro.css";
import"./styles/styleModal/ModalMensaje.css";
import "./styles/stylesPaginaInicio/Pagina_no_encontrada.css"
import "./styles/stylePaginaCliente/NavBarCliente.css";
import "./styles/stylePaginaCliente/PaginaCliente.css";
import  "./styles/styleModal/ModalConfirmar.css";
import "./styles/styleModal/ModalCompras.css";
import "./styles/stylePaginaCliente/NavBarCliente.css";
import "./styles/stylePaginaCliente/PaginaCliente.css";
import "./styles/styleModal/ModalEditarPerfil.css"
import "./styles/stylePaginaAdmin/productosAdmin.css";
import "./styles/styleModal/ModalAdminProductos.css";
import "./styles/stylePaginaAdmin/clientesAdmin.css";

//  utilizamos la libreria ReactDom y su metodo createRoot para poder mostrar el contenido
const root=ReactDom.createRoot(document.getElementById("root"));
root.render(
  // RouterApp es el componente donde estaran todas las rutas definidas
    <RouterApp/>
);