import React,{useState} from "react";
import ModalCompras from "../components/paginaCliente/ModalCompras";
import ModalEditarPerfil from "../components/paginaCliente/ModalEditarPerfil";


import NavBarCliente from "../components/paginaCliente/NavBarCliente";
import ProductoCompra from "../components/paginaCliente/ProductoCompra";
import Footer from "../components/paginaInicio/Foooter";
// import RelojFecha from "../components/RelojFecha";
function PaginaCiente(props) {
    
    return(
        <div className="pagina__cliente">
            {/* <RelojFecha/> */}
            <NavBarCliente/>
            <ProductoCompra
            productos={props.listaProductos}
            />
            <Footer/>
            <ModalCompras/>
            <ModalEditarPerfil/>
        </div>
    );
}
export default PaginaCiente;