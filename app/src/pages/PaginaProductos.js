import NavBarInicio from "../components/paginaInicio/NavBarInicio";
import ProductosInicio from "../components/paginaInicio/ProductosInicio";
import React from "react";
import Footer from "../components/paginaInicio/Foooter";

function PaginaProductos(props) {
    return(
        <div>
            <NavBarInicio/>
            <ProductosInicio
                productos={props.productosGenerales}
            />
            <Footer/>
        </div>
    );
}

export default PaginaProductos;