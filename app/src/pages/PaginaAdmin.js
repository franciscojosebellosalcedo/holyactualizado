import React from "react";
import NavBarAdmin from "../components/paginaAdmin/NavBarAdmin";
import Footer from "../components/paginaInicio/Foooter";


function PaginaAdmin() {
    
    return(
        //debe ser relativo.
        <div className="pagina__admin">
            <NavBarAdmin/>
            <div className="container__img__admin">
                <div className="img__graficas">
                    <div className="imagen1"></div>
                    <div className="imagen2"></div>
                </div>
            </div>
            <Footer/>
        </div>
    );
}
export default PaginaAdmin;