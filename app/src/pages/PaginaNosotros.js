import Footer from "../components/paginaInicio/Foooter";
import NavBarInicio from "../components/paginaInicio/NavBarInicio";
import Nosotros from "../components/paginaInicio/Nosotros";

function PaginaNosotros() {
    return(
        <div>
            <NavBarInicio/>
            <Nosotros/>
            <Footer/>
        </div>
    );
}

export default PaginaNosotros;