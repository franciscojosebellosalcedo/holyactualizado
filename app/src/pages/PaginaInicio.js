import React from "react";
import ModalCompras from "../components/paginaCliente/ModalCompras";
import ContenidoInicio from "../components/paginaInicio/ContenidoInicio";
import Footer from "../components/paginaInicio/Foooter";
import ModalMensaje from "../components/modal/ModalMensaje";
import NavBarInicio from "../components/paginaInicio/NavBarInicio";
//importamos los componentes que tendra la el componente pagina PaginaInicio


//creamos el componente pagina PaginaInicio
function PaginaInicio() {
    return(
        //mostramos en el navegador los componentes importados
        <div className="containerPaginaInicio">
            <NavBarInicio/>
            <ContenidoInicio/>
            <Footer/>
        </div>
    );
}

//exportamos el componente para luego importarlo en donde nosotros querramos
export default PaginaInicio;