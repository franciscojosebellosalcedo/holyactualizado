import { Link } from "react-router-dom";
//creamos el componente de PaginaNoEncontrada para asignar una vista en caso de que una ruta no exista
function PaginaNoEncontrada() {
    return (
        //inicialmente se esta mostrando en el navegador una div con el texto Pagina No Encontrada 😔 
        <div className="pagina">
            <h1 className="pagina-titulo">PAN-gina No Econtrada</h1>
            <div className="pagina-img">
                <img className="img-pan" src={require("../img/panTriste.png")} alt="" />
            </div>
            <Link to="/" className="pagina-inicio">Ir al inicio</Link>

        </div>
    );
}
//exportamos el componente para luego importarlo en donde nosotros querramos
export default PaginaNoEncontrada;


