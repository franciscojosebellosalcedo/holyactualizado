
function ModalConfirmar() {
   
    function cerrarMensaje() {
        const modalConfirmar=document.querySelector(".cont_modal_confirmar");
        modalConfirmar.classList.remove("mostrar_modal_confirmar");
        
     }
   
   
    return(
    <div className="cont_modal_confirmar " >
        <div className="modal_confirmar">
        <div className="relativo">
            <h2 className="modal_confirmar_titulo">what ever</h2>
            <p className="modal_texto">¿Lorem ipsum dolor sit amet consectetur.?</p>
        </div>
        <div className="modal_botones">
            <button className="modal_btn btn_si" >Si</button>
            <button className="modal_btn  btn_no"onClick={()=>cerrarMensaje()}>No</button>
        </div>

        </div>
    </div>
   );
    

}
export default ModalConfirmar;