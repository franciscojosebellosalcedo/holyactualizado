import React, { useState } from "react";
import ProductosAdmin from "../paginaAdmin/ProductosAdmin";
import ModalMensaje from "./ModalMensaje";
function ModalLogin(props) {
  const [usuario, setUsuario] = useState({ correo: "", contrasenia: "" });
  function guardarUsuario(e) {
    setUsuario({ ...usuario, [e.target.name]: e.target.value });
  }

  function validarCamposLogin() {
    const { correo, contrasenia } = usuario;
    if (correo === "" || contrasenia === "") {
      return true;
    } else {
      return false;
    }
  }
  function verContrasena(e) {
    guardarUsuario(e);
    let input = document.getElementById("modal-input-contrasena");
    let img = document.querySelector(".ocultar");
    let inputType = input.type;
    let inputValue = input.value;
    if (inputType === "password") {
      img.classList.add("img-ver");
    } else if (inputType === "text") {
      img.classList.add("ojo-abierto");
    }
    if (inputValue === "") {
      img.classList.remove("ojo-abierto");
      img.classList.remove("img-ver");
      input.type = "password";
    }
  }
  function mostrarString() {
    const input = document.querySelector("#modal-input-contrasena");
    let img = document.querySelector(".ocultar");
    const tipoInput = input.type;
    if (tipoInput === "password") {
      input.type = "text";
      img.classList.remove("img-ver");
      img.classList.add("ojo-abierto");
    } else if (tipoInput === "text") {
      input.type = "password";
      img.classList.remove("ojo-abierto");
      img.classList.add("img-ver");
    }
  }
  function cerrarModal() {
    const modal_login = document.querySelector(".container-modal");
    modal_login.classList.remove("mostrar-login");
  }
  function registrate(e) {
    e.preventDefault();
    cerrarModal();
    const modal_registro = document.querySelector(".container-modal-registro");
    modal_registro.classList.add("mostrar-modal-registro");
  }
  function iniciarSesion(e) {
    e.preventDefault();
    if (validarCamposLogin()) {
        props.fnMensaje("Llene los campos por favor.");
        props.fnModalMensaje();
    }
  }
  return (
    <div className="container-modal">
      <ModalMensaje />
      <div className="modal">
        <h3 className="modal-titulo">Login</h3>
        <p className="modal-salir" onClick={() => cerrarModal()}>
          X
        </p>
        <form className="modal-formulario">
          <div className="caja-1">
            <div className="container__input__caja">
              <label
                htmlFor="modal-input-correo"
                className="modal-user"
              ></label>
              <input
                onChange={guardarUsuario}
                type="email"
                name="correo"
                value={usuario.correo}
                id="modal-input-correo"
                placeholder="Correo"
                className="input"
              />
            </div>
          </div>

          <div className="caja-2">
            <div className="container__input__caja">
              <label
                htmlFor="modal-input-contrasena"
                className="modal-contrasena "
              ></label>
              <input
                onChange={verContrasena}
                value={usuario.contrasenia}
                type="password"
                name="contrasenia"
                id="modal-input-contrasena"
                placeholder="Contraseña"
                className="input"
              />
              <label
                className="ocultar"
                onClick={() => mostrarString()}
              ></label>
            </div>
          </div>

          <div className="caja-3">
            <button
              className="modal-btn-ingresar login-btn"
              onClick={(e) => iniciarSesion(e)}
            >
              Iniciar{" "}
            </button>
            <button
              className="modal-btn-cuenta login-btn"
              onClick={(e) => registrate(e)}
            >
              Crear cuenta
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
export default ModalLogin;
