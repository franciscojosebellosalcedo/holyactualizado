
function ModalEditar() {
     function cerrar() {
       const moverModalEditar= document.querySelector(".editar");
        moverModalEditar.classList.remove("mostrar_modal_admin_productos");
     }
    return (
        <div className="contenedor_modal_agregar  editar  ">
            <div className="modal_agregar_admin">
                <h3 className="modal-agregar-titulo">Actualizar Producto</h3>
                <p className="modal_agregar_salir" onClick={()=>cerrar()} >X</p>

                <form className="modal_agregar_formulario_admin">
                    <div className="cont_agregar_registro_admin">

                        <div className="cont__registro_admin">
                            <div className="cont_registro">
                                <label className="modal_agregar_nombre_label">Nombre</label>
                                <input id="input-nombre" type="text" className=" input" />
                            </div>
                            <div className="cont_registro">
                                <label className="modal_agregar_nombre_label">Descripcion</label>
                                <input id="input-Descipcion" type="text" className=" input" />
                            </div>

                        </div>

                        <div className="cont__registro_admin">
                            <div className="cont_registro">
                                <label className="modal_agregar_nombre_label">Precio</label>
                                <input id="input-precio" type="number" className=" input" />
                            </div>
                            <div className="cont_registro">
                                <label className="modal_agregar_nombre_label">Cantidad</label>
                                <input id="input-cantidad" type="number" className=" input" />
                            </div>

                        </div>

                    </div>

                    <div className="cont_agregar_btn">
                        <button className="btn_agregar_admin btn_cancelar" >Cancelar</button>
                        <button className="btn_agregar_admin btn_actualizar" >Actualizar</button>
                    </div>

                </form>

            </div>
        </div>

    );
}
 export default ModalEditar;
