import React, { useState, useEffect } from "react";
import ModalMensaje from "./ModalMensaje";
function ModalRegistro(props) {
  const [usuario, setUsuario] = useState({
    correo: "",
    contrasenia: "",
  });
  const [cliente, setCliente] = useState({
    nombres: "",
    apellidos: "",
    fecha_nacimiento: "",
    documento: "",
    telefono: "",
    direccion: "",
    sexo: "",
  });

  useEffect(() => {
    setCliente({ ...cliente, usuario: usuario });
  }, [usuario]);

  useEffect(() => {
    localStorage.setItem("clientes", JSON.stringify(props.clientes));
  }, [props.clientes]);

  function guardarDatosCliente(e) {
    setCliente({ ...cliente, [e.target.name]: e.target.value });
  }
  function validarCamposRegistro() {
    const {
      nombres,
      apellidos,
      fecha_nacimiento,
      documento,
      telefono,
      direccion,
      sexo,
    } = cliente;
    const {correo,contrasenia}=usuario;
    if (
      nombres === "" ||
      apellidos === "" ||
      fecha_nacimiento === "" ||
      documento === "" ||
      telefono === "" ||
      direccion === "" ||
      sexo === "" ||
      correo==="" ||
      contrasenia === ""
    ) {
      return true;
    } else {
      return false;
    }
  }
  

  function volver(e) {
    e.preventDefault();
    const modal_registro = document.querySelector(".container-modal-registro");
    modal_registro.classList.remove("mostrar-modal-registro");
    const modal_login = document.querySelector(".container-modal");
    modal_login.classList.add("mostrar-login");
  }
  function limpiarCampos() {
    setCliente({
      ...cliente,
      nombres: "",
      apellidos: "",
      fecha_nacimiento: "",
      documento: "",
      telefono: "",
      direccion: "",
      sexo: "",
    });
    const sexo = document.getElementById("select-sexo");
    sexo.selectedIndex = 0;
    setUsuario({ ...usuario, correo: "", contrasenia: "" });
    let img = document.querySelector(".ocultar-registro");
    img.classList.remove("ojo-abierto-registro");
    img.classList.remove("img-ver-registro");
    let input = document.querySelector(".modal-input-contrasena-registro");
    input.type = "password";
  }
  function cierre() {
    const modal_registo = document.querySelector(".container-modal-registro");
    modal_registo.classList.remove("mostrar-modal-registro");
  }
  function guardarUsuario(e) {
    e.preventDefault();
    setUsuario({ ...usuario, [e.target.name]: e.target.value });
    console.log(usuario);
  }
  function verContrasena(e) {
    guardarUsuario(e);
    let input = document.querySelector(".modal-input-contrasena-registro");
    let img = document.querySelector(".ocultar-registro");
    let inputType = input.type;
    let inputValue = input.value;
    if (inputType === "password") {
      img.classList.add("img-ver-registro");
    } else if (inputType === "text") {
      img.classList.add("ojo-abierto-registro");
    }
    if (inputValue === "") {
      input.type = "password";
      img.classList.remove("ojo-abierto-registro");
      img.classList.remove("img-ver-registro");
    }
  }
  function mostrarString() {
    const input = document.querySelector(".modal-input-contrasena-registro");
    let img = document.querySelector(".ocultar-registro");
    const tipoInput = input.type;
    if (tipoInput === "password") {
      input.type = "text";
      img.classList.remove("img-ver-registro");
      img.classList.add("ojo-abierto-registro");
    } else if (tipoInput === "text") {
      input.type = "password";
      img.classList.remove("ojo-abierto-registro");
      img.classList.add("img-ver-registro");
    }
  }
  function guardarCliente(e) {
    e.preventDefault();
 
    if (validarCamposRegistro()) {
      props.fnMensaje("Llene los campos por favor.");
      props.fnModalMensaje();
    } else {
      let lista = props.clientes;
      props.fnSetClientes([...lista, cliente]);
      limpiarCampos();
      props.fnMensaje("Registro exitoso.");
        props.fnModalMensaje();
      console.log(props.clientes);
    }
  }
  return (
    <div className="container-modal-registro">
      <ModalMensaje />
      <div className="modal-registro">
        <h3 className="modal-registro-titulo">Datos Personales</h3>
        <p className="modal-salir" onClick={() => cierre()}>
          X
        </p>
        <form className="modal-registros">
          <div className="caja-registros">
            <div className="caja__container">
              <div className="caja__registro">
                <label
                  htmlFor="input-nombres"
                  className="modal-nombres label-modal-registro"
                >
                  Nombres
                </label>
                <input
                  onChange={guardarDatosCliente}
                  value={cliente.nombres}
                  name="nombres"
                  id="input-nombres"
                  type="text"
                  className=" input"
                />
              </div>

              <div className="caja__registro">
                <label
                  htmlFor="input-apellidos"
                  className="modal-apellidos label-modal-registro"
                >
                  Apellidos
                </label>
                <input
                  onChange={guardarDatosCliente}
                  value={cliente.apellidos}
                  name="apellidos"
                  id="input-apellidos"
                  type="text"
                  className="modal-apellidos input-registro input"
                />
              </div>
            </div>

            <div className="caja__container">
              <div className="caja__registro">
                <label
                  htmlFor="input-fecha-nacimiento"
                  className="modal-nacimiento label-modal-registro"
                >
                  Fecha Nacimiento
                </label>
                <input
                  onChange={guardarDatosCliente}
                  value={cliente.fecha_nacimiento}
                  name="fecha_nacimiento"
                  id="input-fecha-nacimiento"
                  type="date"
                  className="modal-nacimiento input-registro input"
                />
              </div>
              <div className="caja__registro">
                <label
                  htmlFor="input-documento"
                  className="modal-documento label-modal-registro"
                >
                  Documento
                </label>
                <input
                  onChange={guardarDatosCliente}
                  value={cliente.documento}
                  name="documento"
                  type="number"
                  id="input-documento"
                  className="modal-documento input-registro input"
                />
              </div>
            </div>
            <div className="caja__container">
              <div className="caja__registro">
                <label
                  htmlFor="input-telefono"
                  className="modal-telefono label-modal-registro"
                >
                  Telefono
                </label>
                <input
                  onChange={guardarDatosCliente}
                  value={cliente.telefono}
                  name="telefono"
                  type="tel"
                  className="modal-telefono input-registro input"
                  id="input-telefono"
                />
              </div>
              <div className="caja__registro">
                <label
                  htmlFor="input-direccion"
                  className="modal-direccion label-modal-registro"
                >
                  Dirección
                </label>
                <input
                  onChange={guardarDatosCliente}
                  value={cliente.direccion}
                  name="direccion"
                  id="input-direccion"
                  type="text"
                  className="modal-direccion input-registro input"
                />
              </div>
            </div>
            <div className="caja__container">
              <div className="caja__registro caja--select">
                <label
                  htmlFor="select-sexo"
                  className="modal-sexo label-modal-registro"
                >
                  Sexo
                </label>
                <select
                  onChange={guardarDatosCliente}
                  name="sexo"
                  id="select-sexo"
                  className="modal-selec input-registro input"
                >
                  <option value="MASCULINO">Elige</option>
                  <option value="MASCULINO">Masculino</option>
                  <option value="FEMENINO">Femenino</option>
                  <option value="OTROS">Otros</option>
                </select>
              </div>
            </div>

            <div className="caja__container caja--top">
              <div className="caja__registro">
                <label htmlFor="input-correo" className="modal-user "></label>
                <div className="caja__input">
                  <input
                    onChange={guardarUsuario}
                    value={usuario.correo}
                    name="correo"
                    type="email"
                    id="input-correo"
                    placeholder="Correo"
                    className="input"
                  />
                </div>
              </div>
              <div className="caja__registro">
                <label
                  htmlFor="input-contrasenia"
                  className="modal-contrasena"
                ></label>
                <div className="caja__input">
                  <input
                    onChange={verContrasena}
                    value={usuario.contrasenia}
                    type="password"
                    id="input-contrasenia"
                    placeholder="Contraseña"
                    name="contrasenia"
                    className="input modal-input-contrasena-registro"
                  />
                  <label
                    onClick={() => mostrarString()}
                    className="ocultar-registro"
                  ></label>
                </div>
              </div>
            </div>

            <div className="caja__container cj_btn">
              <button
                className="modal-btn-ingresar registro-btn"
                onClick={(e) => {
                  volver(e);
                }}
              >
                Volver
              </button>
              <button
                onClick={(e) => guardarCliente(e)}
                className="modal-btn-cuenta registro-btn"
              >
                Registrate
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
export default ModalRegistro;
