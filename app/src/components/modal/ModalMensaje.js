
function ModalMensaje(props) {

 function quitarMensaje() {
  const modalMensaje=document.querySelector(".container-mensaje");
  modalMensaje.classList.remove("mostrar-mensaje");
 }
 
 
  return(
  
    <div className="container-mensaje">
    <div className="modal-mensaje">
      <p className="modal-salir-mensaje" onClick={()=>quitarMensaje()}>X</p>
      <h2 className="modal-titulo-mensaje">Mensaje</h2>
      <p className="modal-infor-mensaje">
        {props.mensaje} 
      </p>
  </div>
  </div>
  
)
    


}
export default ModalMensaje;