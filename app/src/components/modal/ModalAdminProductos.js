function ModalAgregar() {
    function cerrar() {
        const moverModalAgregar= document.querySelector(".agregar");
        moverModalAgregar.classList.remove("mostrar_modal_admin_productos");
    }
    return (
        <div className="contenedor_modal_agregar agregar  ">
            <div className="modal_agregar_admin">
                <h3 className="modal-agregar-titulo">Agregar Producto</h3>
                <p className="modal_agregar_salir" onClick={() => cerrar()} >X</p>

                <form className="modal_agregar_formulario_admin">
                    <div className="cont_agregar_registro_admin">

                        <div className="cont__registro_admin">
                            <div className="cont_registro">
                                <label className="modal_agregar_nombre_label">Nombre</label>
                                <input id="input-nombre" type="text" className=" input" />
                            </div>
                            <div className="cont_registro">
                                <label className="modal_agregar_nombre_label">Descripcion</label>
                                <input id="input-Descipcion" type="text" className=" input" />
                            </div>

                        </div>

                        <div className="cont__registro_admin">
                            <div className="cont_registro">
                                <label className="modal_agregar_nombre_label">Precio</label>
                                <input id="input-precio" type="number" className=" input" />
                            </div>
                            <div className="cont_registro">
                                <label className="modal_agregar_nombre_label">Cantidad</label>
                                <input id="input-cantidad" type="number" className=" input" />
                            </div>

                        </div>

                    </div>

                    <div className="cont_agregar_btn">
                        <button className="btn_agregar_admin" >Guardar</button>
                    </div>

                </form>

            </div>
        </div>

    );
}
export default ModalAgregar;