import React from "react";

function ModalCompras(props) {
 function cerrarModal() {
    const modal=document.querySelector(".container__modal__compras");
    modal.classList.remove("mostrar__modal__compras");
 }
   return(
    <div className="container__modal__compras ">
        <div className="modal__compras">
        <p className="modal__salir__compras" onClick={()=>cerrarModal()}>X</p>
        <h3 className="titulo__modal">Ultima Compra</h3>
        <form className="modal__formulario__compras">
                    <div className="">
                        <div >
                            <label >Nombre: </label>
                            <label className="label__detalle" >{}</label>
                        </div>
                    </div>

                    <div>
                        <div>
                        <label >Producto: </label>
                        <label className="label__detalle">{}</label>
                        </div>
                    </div>
                    <div >
                        <div >
                        <label >Precio: </label>
                        <label className="label__detalle">{}</label>
                        </div>
                    </div>
                    <div >
                        <div >
                        <label >Tipo de compra: </label>
                        <label className="label__detalle">{}</label>
                        </div>
                    </div>
                    <div >
                        <div >
                        <label >Cantidad: </label>
                        <label className="label__detalle">{}</label>
                        </div>
                    </div>
                    <div >
                        <div >
                        <label >Sub Total: </label>
                        <label className="label__detalle">{}</label>
                        </div>
                    </div>
                    <div >
                        <div >
                        <label >Total: .......................................................................... {}</label>
                        <label className="label__detalle">{}</label>
                        </div>
                    </div>
                </form>
            
        </div>

    </div>
   ); 
}
export default ModalCompras;