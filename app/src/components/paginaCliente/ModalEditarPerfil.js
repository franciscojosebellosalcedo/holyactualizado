import React from "react";
function ModalEditarPerfil() {
  function cerrarModal() {
    const modal=document.querySelector(".container__editar__perfil");
    modal.classList.remove("mostrar__modal__editar__perfil");
 }
  return (
    <div className="container__editar__perfil  ">
      <div className="modal__editar__perfil">
        <p  className="modal__salir__compras" onClick={()=>cerrarModal()}>X</p>
        <h2 className="titulo__modal__editar__perfil">Perfil</h2>
        <form className="modal__formulario__editar">
          <div className="container__registros">
            <div className="caja__container">

              <div className="caja__registro">
                <label for="input-nombres" className="contanier__nombre">Nombres</label>
                <input id="input-nombres" type="text" className="input"/>
              </div>

              <div className="caja__registro">
                <label  for="input-apellidos" className="contanier__apellidos">Apellidos</label>
                <input id="input-apellidos" type="text" className="input" />
              </div>
            </div>
            <div className="caja__container">
            <div className="caja__registro">
               <label for="input-fecha-nacimiento"className="modal-nacimiento label-modal-registro">Fecha Nacimiento</label>
               <input id="input-fecha-nacimiento" type="date" className="modal-nacimiento input-registro input" />
           </div>
           <div className="caja__registro">
              <label for="input-documento" className="modal-documento label-modal-registro">Documento</label>
              <input  id="input-documento"type="number" className="modal-documento input-registro input" />
           </div>
           </div>
           <div className="caja__container">
           <div className="caja__registro">
           <label for="input-telefono "className="modal-telefono label-modal-registro">Telefono</label>
            <input id="input-telefono "type="number"  className="modal-telefono input-registro input" />
           </div>
            <div className="caja__registro">
            <label for="input-direccion"className="modal-direccion label-modal-registro">Dirección</label>
            <input id="input-direccion" type="text" className="modal-direccion input-registro input" />
            </div>
           </div>
           <div className="caja__container">
              <div className="caja__registro caja--select">
                  <label for="input-sexo"className="modal-sexo label-modal-registro">Sexo</label>
                  <select id="input-sexo"className="modal-selec input-registro input">
                      <option value="MASCULINO">Masculino</option>
                      <option value="FEMENINO">Femenino</option>
                    <option value="OTROS">Otros</option>
              </select>
            </div>
            </div>
           <div className="caja__container caja--top">
              <div className="caja__registro">
                <label for="modal-input-correo" className="modal-user "></label>
                <div className="caja__input">
                <input type="email" id="modal-input-correo" placeholder="Correo" className="input"/>
                </div>
              </div>
              <div className="caja__registro">
                <label for="modal-input-contrasena-registro" className="modal-contrasena"></label>
                <div className="caja__input">
                  <input  type="password" id="modal-input-contrasena-registro" placeholder="Contraseña" className="input"/>
                  <label  className="ocultar-registro"></label>
                </div>
              </div>
            </div>
          </div>
        </form>
        <div className="container__btn">
                <button onClick={()=>cerrarModal()} className="btn-modal boton1">Cancelar</button>
                <button className="btn-modal boton2">Actualizar</button>
              </div>
      </div>
    </div>
  );
}
export default ModalEditarPerfil;
