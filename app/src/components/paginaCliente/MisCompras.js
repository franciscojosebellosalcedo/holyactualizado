import React from "react";
import NavBarCliente from "./NavBarCliente";
import Foooter from "../paginaInicio/Foooter";
import ModalCompras from "./ModalCompras";
import ModalEditarPerfil from "./ModalEditarPerfil";
function MisCompras() {
    return(
        <div className="compras">
            <NavBarCliente/>
            <div className="compras__informacion">
                <h1 className="compras__nombre__cliente">Francisco jose bello</h1>
                <div className="container__tabla__mis__compras">
                    <table>
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <ModalCompras/>
            <ModalEditarPerfil/>
            <Foooter/>
        </div>
    );
}
export default MisCompras;