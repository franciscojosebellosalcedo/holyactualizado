import React from "react";
import {NavLink,Link  } from "react-router-dom";
function NavBarCliente() {
    function mostrarModalUltimaCompra(e) {
        const modal=document.querySelector(".container__modal__compras");
        modal.classList.add("mostrar__modal__compras");
        e.preventDefault();
        
     }
     function mostrarModalEditarPerfil(e) {
        const modalPerfil=document.querySelector(".container__editar__perfil");
        modalPerfil.classList.add("mostrar__modal__editar__perfil");
        e.preventDefault();
     }

    return(
        <header className="header">
        <nav className="header-nav">
           <Link to="/cliente/inicio"> <img className="header-icon" src={require("../../icon/holyIcon.jpeg")} alt="icon"/></Link>
            <h2 className="header-titulo">El Pan De Cada Día</h2>

            <input type="checkbox"  id="check" />
            <label for="check" className="menu">
                <div className="linea linea-arriba"></div>
                <div className="linea linea-medio"></div>
                <div className="linea linea-abajo"></div>
            </label>

            <ul className="header-lista">
                <li className="header-item"><a href="/" onClick={(e)=> mostrarModalUltimaCompra(e)}  className="btn__mostrar__compra">Ultima Compra</a></li>
                <li className="header-item"><NavLink  id="nosotros" to="/cliente/mis_compras">Mis compras</NavLink></li>
                <div className="container__icon">
                    <div className="icono__perfil">
                        <ul className="nav_vertical">
                            <li><Link to={"#"} className="item__perfil" onClick={(e)=>mostrarModalEditarPerfil(e)}>Perfil</Link></li>
                            <li><Link to={"#"}>Cerrar sesion</Link></li>
                        </ul>
                    </div>
                </div>
            </ul>
        </nav>

    </header>
    );
}

export default NavBarCliente;