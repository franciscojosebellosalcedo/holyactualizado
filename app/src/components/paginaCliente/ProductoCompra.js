import React,{ useState} from "react";
import ProductoVenta from "./ProductoVenta";
function ProductoCompra(props) {
	const [valor,setValor]=useState("");
	const [lista,setLista]=useState(props.productos);

	function filtro() {
		let encontrados=[];
		lista.forEach((producto)=>{
			if (producto.nombre.trim().toLowerCase()===valor.trim().toLowerCase() || producto.precio===valor.trim().toLowerCase()) {
				encontrados.push(producto);
			}
		});
		if(encontrados.length>0){
			return encontrados;
		}else if(lista.length>0){
			return lista;
		}else{
			return <h1>No hay productos</h1>
		}
	}

	return (
		<div className="container-productos container__productos__venta">

			<div className="container__lista__productos">
				<h1 className="titulo__producto__venta">
					Productos 
					<div>
						<form className="nav__formulario">
							<input onChange={(e)=>setValor(e.target.value)} className="input" placeholder="buscar nombre" id="nav_busqueda" type="search" />
						</form>
					</div>
					
				</h1>
				<div className="productos__flex">
					{
						
						filtro().length>0 ?
						filtro().map((producto,index)=>(
							<ProductoVenta
								producto={producto}
								index={index}
							/>
						))
						:
						filtro()
						
						
					}
				</div>
			</div>

			<div className="container__detalles ">
				<h1 className="titulo__producto__venta">Detalles</h1>
				<div>
					<table className="tabla">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Precio</th>
								<th>Tipo compra</th>
								<th>Cantidad</th>
								<th>Sub total</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>

							<tr className="tr__producto">

								<td>pan queso</td>
								<td>1000</td>
								<td>canasta</td>
								<td>2</td>
								<td>2000</td>
								<td>
									<button className="btn__detalle btn__detalle__editar">Editar</	button>
									<button className="btn__detalle btn__detalle__eliminar">Eliminar</button>
								</td>
							</tr>
							<tr className="tr__producto">

								<td>pan queso</td>
								<td>1000</td>
								<td>canasta</td>
								<td>2</td>
								<td>2000</td>
								<td>
									<button className="btn__detalle btn__detalle__editar">Editar</	button>
									<button className="btn__detalle btn__detalle__eliminar">Eliminar</button>
								</td>
							</tr>
							<tr className="tr__producto">

								<td>pan queso</td>
								<td>1000</td>
								<td>canasta</td>
								<td>2</td>
								<td>2000</td>
								<td>
									<button className="btn__detalle btn__detalle__editar">Editar</	button>
									<button className="btn__detalle btn__detalle__eliminar">Eliminar</button>
								</td>
							</tr>
							<tr className="tr__producto">

								<td>pan queso</td>
								<td>1000</td>
								<td>canasta</td>
								<td>2</td>
								<td>2000</td>
								<td>
									<button className="btn__detalle btn__detalle__editar">Editar</	button>
									<button className="btn__detalle btn__detalle__eliminar">Eliminar</button>
								</td>
							</tr>
							<tr className="tr__producto">

								<td>pan queso</td>
								<td>1000</td>
								<td>canasta</td>
								<td>2</td>
								<td>2000</td>
								<td>
									<button className="btn__detalle btn__detalle__editar">Editar</	button>
									<button className="btn__detalle btn__detalle__eliminar">Eliminar</button>
								</td>
							</tr>
							<tr className="tr__producto">

								<td>pan queso</td>
								<td>1000</td>
								<td>canasta</td>
								<td>2</td>
								<td>2000</td>
								<td>
									<button className="btn__detalle btn__detalle__editar">Editar</	button>
									<button className="btn__detalle btn__detalle__eliminar">Eliminar</button>
								</td>
							</tr>
							<tr className="tr__producto">

								<td>pan queso</td>
								<td>1000</td>
								<td>canasta</td>
								<td>2</td>
								<td>2000</td>
								<td>
									<button className="btn__detalle btn__detalle__editar">Editar</	button>
									<button className="btn__detalle btn__detalle__eliminar">Eliminar</button>
								</td>
							</tr>
							<tr className="tr__producto">

								<td>pan queso</td>
								<td>1000</td>
								<td>canasta</td>
								<td>2</td>
								<td>2000</td>
								<td>
									<button className="btn__detalle btn__detalle__editar">Editar</	button>
									<button className="btn__detalle btn__detalle__eliminar">Eliminar</button>
								</td>
							</tr>
							<tr className="tr__producto">

								<td>pan queso</td>
								<td>1000</td>
								<td>canasta</td>
								<td>2</td>
								<td>2000</td>
								<td>
									<button className="btn__detalle btn__detalle__editar">Editar</	button>
									<button className="btn__detalle btn__detalle__eliminar">Eliminar</button>
								</td>
							</tr>
							<tr className="tr__producto">

								<td>pan queso</td>
								<td>1000</td>
								<td>canasta</td>
								<td>2</td>
								<td>2000</td>
								<td>
									<button className="btn__detalle btn__detalle__editar">Editar</	button>
									<button className="btn__detalle btn__detalle__eliminar">Eliminar</button>
								</td>
							</tr>
							<tr className="tr__producto">

								<td>pan queso</td>
								<td>1000</td>
								<td>canasta</td>
								<td>2</td>
								<td>2000</td>
								<td>
									<button className="btn__detalle btn__detalle__editar">Editar</	button>
									<button className="btn__detalle btn__detalle__eliminar">Eliminar</button>
								</td>
							</tr>
							<tr className="tr__producto">

								<td>pan queso</td>
								<td>1000</td>
								<td>canasta</td>
								<td>2</td>
								<td>2000</td>
								<td>
									<button className="btn__detalle btn__detalle__editar">Editar</	button>
									<button className="btn__detalle btn__detalle__eliminar">Eliminar</button>
								</td>
							</tr>
							<tr className="tr__producto">

								<td>pan queso</td>
								<td>1000</td>
								<td>canasta</td>
								<td>2</td>
								<td>2000</td>
								<td>
									<button className="btn__detalle btn__detalle__editar">Editar</	button>
									<button className="btn__detalle btn__detalle__eliminar">Eliminar</button>
								</td>
							</tr>



						</tbody>

					</table>
				</div>

				<div className="container__total">
					<div className="detalle__info">
						<h2 className="detalle-total">Total: <span className="detalle-monto">$ 3.000</span></h2>
						<button className="btn-detalle">Confirmar Compra</button>
					</div>
					 ipsum dolor sit amet consectetur adipisicing elit. Dignissimos provident doloremque maiores consequatur id voluptas
				</div>
			</div>


		</div>
	);
}
export default ProductoCompra;