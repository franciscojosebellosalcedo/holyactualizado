import React from "react";
function ProductoVenta(props) {
    return (
        <div className="card card--producto--venta">
          <h1 className="titulo" >{props.producto.nombre}</h1>
          <div className="card-img">
            <img className="card-img-producto" src={require(`../../img/pan_${props.index+1}.jpeg`)} alt="imagen" />
          </div>
          <div className="card-descripcion">{props.producto.descripcion}</div>
          <div className="card-precio">
            <span className="precio">Precio :</span>${props.producto.precio}
          </div>
          <div className="card__select">
            <select id="producto__select">
              <option value="">Seleccione catidad</option>
              <option value="canasta">Canasta</option>
              <option value="unidad">Unidad</option>
            </select>
          </div>
          <form className="form__cantidad">
            <input id ="producto__cantidad--input"className="input" type="number" placeholder="Cantidad"/>
            <div className="icon__shop"></div>
          </form>
        </div>
      );
}
export default ProductoVenta;