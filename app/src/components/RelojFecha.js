import React,{useEffect, useState} from "react";
function RelojFecha() {
    const [time,setTime]=useState({
        hora:"",
        minutos:"",
        segundos:"",
        pmAm:"",
        dia:"",
        mes:"",
        anio:"",
    });
    useEffect(()=>{
        setInterval(()=>{
            let fecha=new Date();
            setTime({...time,hora:fecha.getHours(),
            minutos:fecha.getMinutes(),
            segundos:fecha.getSeconds(),
            dia:fecha.getDate(),
            mes:fecha.getMonth()+1,
            anio:fecha.getFullYear(),
            });  
        },
        1000); 
        
    },[ ])
    function mostrarFechaHora() { 
        const container=document.querySelector(".container__fixed");
        const icon_flecha=document.querySelector(".icono");
        container.classList.toggle("trasladar__container");
        if (icon_flecha.classList.contains("icono__flecha__left")) {
            icon_flecha.classList.remove("icono__flecha__left");
            icon_flecha.classList.add("icono__flecha__right");
            container.classList.add("mostrar__fecha__hora");
        }else if (icon_flecha.classList.contains("icono__flecha__right")){
            icon_flecha.classList.remove("icono__flecha__right");
            icon_flecha.classList.add("icono__flecha__left");
            container.classList.remove("mostrar__fecha__hora");
        }
    }
    return(
        <div className="container__fixed ">
            <div onClick={()=>mostrarFechaHora()} className="icono__flecha__left icono"></div>
            <div className="icono__reloj">
                <div className="container__fecha__hora">
                    <p className="fecha texto">{time.dia}/{time.mes}/{time.anio}</p>
                    <p className="hora texto">{time.hora}:{time.minutos}:{time.segundos} {time.pmAm}</p>
                </div>
            </div>
            
        </div>
    );
}
export default RelojFecha;