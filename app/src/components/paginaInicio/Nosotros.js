function Nosotros() {
    return(
        <div className="container-nosotros">
            <img src={require("../../img/nosotros.jpeg")} alt="" className="nosotros-img" /> 
            <div className="nosotros-info">
                <div className="nosotros-mision">
                    <h3 className="titulo ">Misión</h3>
                    <p className="nosotros-mision-parrafo">
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reprehenderit, provident blanditiis. Perspiciatis quos distinctio reiciendis quae, doloremque ipsam architecto voluptatibus nam quisquam non dolores molestias soluta blanditiis autem incidunt quis.
                    </p>
                </div>
                <div className="nosotros-vision">
                    <h3 className=" titulo ">Visión</h3>
                    <p className="nosotros-vision-parrafo">
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reprehenderit, provident blanditiis. Perspiciatis quos distinctio reiciendis quae, doloremque ipsam architecto voluptatibus nam quisquam non dolores molestias soluta blanditiis autem incidunt quis.
                    </p>
                </div>
            </div>
        </div>
    );
}
export default Nosotros;