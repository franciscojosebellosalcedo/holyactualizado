/*
importamos la libreria react-roter-dom para hacer uso de su componente NavLink que nos 
permite hacer una barra de navegacion sin que la pagina se refrezque
*/
import { NavLink,Link } from "react-router-dom";
import React,{  useState} from "react";
//creamos el componente NavBarInicio 

function NavBarInicio() {
    function mostrarModal(){
        const modal_login=document.querySelector(".container-modal");
        modal_login.classList.add("mostrar-login");
    }
    return(
        //mostramos en el navegador el menu de navegacion utilizando NavLink
        <header className="header">
            <nav className="header-nav">
               <Link to="/"> <img className="header-icon" src={require("../../icon/holyIcon.jpeg")} alt="icon"/></Link>
                <h2 className="header-titulo">El Pan De Cada Día</h2>

                <input type="checkbox"  id="check" />
                <label for="check" className="menu">
                    <div className="linea linea-arriba"></div>
                    <div className="linea linea-medio"></div>
                    <div className="linea linea-abajo"></div>
                </label>

                <ul className="header-lista">
                    <li className="header-item"><NavLink className={(dato)=>dato.isActive ? "active":""} id="productos" to="/productos">Productos</NavLink></li>
                    <li className="header-item"><NavLink  id="nosotros" to="/nosotros">Nosotros</NavLink></li>
                    <li className="header-item"><a onClick={()=>mostrarModal()} id="inicio_sesion" >Inicio sesion</a></li>
                </ul>
            </nav>

        </header>
    );
}
//exportamos el componente para luego importarlo en donde nosotros querramos
export default NavBarInicio;