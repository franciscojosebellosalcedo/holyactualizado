import Producto from "./Producto";
import React from "react";
function ProductosInicio(props) {
  return (
    <div className="container-productos">
      <div className="grid-productos">
        {
          props.productos.map((producto,index)=>{
            return(
              <Producto
              producto={producto}
              index={index}
            />
            )
          })
        }
      </div>
      
    </div>
    
  );
}
export default ProductosInicio;
