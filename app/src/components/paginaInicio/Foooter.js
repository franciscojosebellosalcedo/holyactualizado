function Footer(){
return(
    <div className="container-footer">
        <div className="footer-info">
            <h3 className="titulo-contactanos">Contáctanos</h3>
            <div className="info-contacto">
                <p className="footer-titulo">Contacto : <span>3104545324</span></p>
                <p className="footer-titulo">Correo : <span>holypan@gmail.com</span></p>
                <p className="footer-titulo">Direccion : <span>San Rafael De La Cruz</span></p>
            </div>
        </div>

        <div className="footer-redes">
            <h3 className="titulo-redes-sociales">Redes Sociales</h3>
           <div className="social-media">

                <div  className="img-facebook red">
                    <a href="#" className="red-social">
                        Facebook</a>
                </div>
                
                <div className=" img-twitter red">
                    <a href="#" className="red-social">
                        Twitter
                </a>
                </div>

                <div className="img-instagram red">
                    <a href="#" className="red-social">
                    Instagram
                </a>    
                </div>
        
           </div>
        </div>
    </div>
)
}
export default Footer;