import React from "react";

function Producto(props) {
  return (
    <div className="card">
      <h1 className="titulo" >{props.producto.nombre}</h1>
      <div className="card-img">
        <img className="card-img-producto" src={require(`../../img/pan_${props.index+1}.jpeg`)} alt="imagen" />
      </div>
      <div className="card-descripcion">{props.producto.descripcion}</div>
    </div>
  );
}
export default Producto;
