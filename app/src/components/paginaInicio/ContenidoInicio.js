import { useEffect } from "react";

//creamos el componente del contenido de la pagina inicial de la app
function ContenidoInicio() {
    //creamos la funcion que se utilizara en el slider
    function siguienteImagen(numero) {
        const lista_slider_body = [...document.querySelectorAll(".slider-body")];
        let valor;
        const elementoActual = Number(document.querySelector(".mostrar").dataset.id);
        valor = elementoActual;
        valor = valor + numero;
        if (valor === 0) {
            valor = lista_slider_body.length;
        } else if (valor === lista_slider_body.length + 1) {
            valor = 1;
        }
        lista_slider_body[elementoActual - 1].classList.toggle("mostrar");
        lista_slider_body[valor - 1].classList.toggle("mostrar");
    }
    function aliderAutomatico() {
        setInterval(() => {
            const lista_slider_body = [...document.querySelectorAll(".slider-body")];
            let valor;
            const elementoActual = Number(document.querySelector(".mostrar").dataset.id);
            valor = elementoActual;
            valor = valor + 1;
            if (valor === 0) {
                valor = lista_slider_body.length;
            } else if (valor === lista_slider_body.length + 1) {
                valor = 1;
            }
            lista_slider_body[elementoActual - 1].classList.toggle("mostrar");
            lista_slider_body[valor - 1].classList.toggle("mostrar");
        },6000);
    }
    useEffect(()=>{
       aliderAutomatico();
    },[ ]);

    return (
        //mostramos en el navegador el contenido que tiene en componente ContenidoInicio
        <div className="contenido">
            <div className="sub-container">
                <div className="contenido-presentacion">
                    <div className="texto">
                        <h3 className="contenido-titulo">Holy Panaderia</h3>
                        <p className="contenido-parrafo">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium nulla nemo illum molestiae nisi itaque ex fuga cum, quae totam.
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium nulla nemo illum molestiae nisi itaque ex fuga cum, quae totam.
                        </p>
                    </div>
                </div>
                <div className="slider">
                    <div onClick={() => siguienteImagen(-1)} >
                        <img className="slider-back btn" src={require("../../icon/rowLeft.png")} alt="icon" />
                    </div>
                    <div className="slider-container">

                        <div className="slider-body mostrar" data-id="1">
                            <img className="slider-img" src={require("../../img/pan_1.jpeg")} alt="imagen" />
                        </div>
                        <div className="slider-body " data-id="2">
                            <img className="slider-img" src={require("../../img/pan_2.jpeg")} alt="imagen" />
                        </div>
                        <div className="slider-body " data-id="3">
                            <img className="slider-img" src={require("../../img/pan_3.jpeg")} alt="imagen" />
                        </div>
                        <div className="slider-body " data-id="4">
                            <img className="slider-img" src={require("../../img/pan_4.jpeg")} alt="imagen" />
                        </div>
                        <div className="slider-body " data-id="5">
                            <img className="slider-img" src={require("../../img/pan_5.jpeg")} alt="imagen" />
                        </div>

                    </div>
                    <div onClick={() => siguienteImagen(1)} >
                        <img className="slider-right btn" src={require("../../icon/rowRight.png")} alt="icon" />
                    </div>
                </div>
            </div>
        </div>
    );
}
//exportamos el componente para luego importarlo en donde nosotros querramos
export default ContenidoInicio;