
import NavBarAdmin from "./NavBarAdmin";
import React from "react";
import Footer from "../paginaInicio/Foooter";
import ModalAgregar from "../modal/ModalAdminProductos"
import ModalEditar from "../modal/ModalEditarProducto";
import ModalConfirmar from "../modal/ModalConfirmar";




function ProductosAdmin() {



    function agregarProductoAdmin(e) {
        e.preventDefault();
        
        const agregar_producto=document.querySelector(".agregar");
        agregar_producto.classList.add("mostrar_modal_admin_productos");

    }
    function editarProductoAdmin(e) {
        e.preventDefault();
        const agregar_producto=document.querySelector(".editar");
        agregar_producto.classList.add("mostrar_modal_admin_productos");

    }
    function eliminarProductoAdmin(e) {
        e.preventDefault();
        const eliminar_producto=document.querySelector(".cont_modal_confirmar");
        eliminar_producto.classList.add("mostrar_modal_confirmar");

    }





    return(
        
        <div className="contenedor_producto_admin">
            <NavBarAdmin />
            <div className="contenedor_producto">
                <div className="contenedor_producto_botonTitulo" >
                    <h1 className="producto_admi_titulo tituloProducto">Productos</h1>
                    <button className="producto_admi_btn_crear" onClick={(e)=>agregarProductoAdmin(e)}>Agregar</button>
                    <input type="text" placeholder="Buscar " className="input input_buscar" />
                </div>  

                 <div className="producto_admi_tabla">
                    <table className="admin_table">
                    <thead>
                        <tr>
                            <th>Codigo</th>
                            <th>Nombre</th>
                            <th>Descripcion</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th>Acciones</th>
                        </tr>
					</thead>
                    <tbody>

                        <tr className="tr__producto_admin">

                            <td>001</td>
                            <td>BocaQueso</td>
                            <td>BocaQueso</td>
                            <td>1000</td>
                            <td>10</td>
                            <td>
                                <button className="btn__producto_admin btn__producto_admin__editar" onClick={(e)=>editarProductoAdmin(e)}>Editar</	button>
                                <button className="btn__producto_admin btn__producto_admin__eliminar" onClick={(e)=>eliminarProductoAdmin(e)} >Eliminar</button>
                            </td>
                        </tr>
                        

                        
                     </tbody>
                    </table>

                 </div>

            </div>
              <ModalEditar/>  
              <ModalAgregar/>   
              <ModalConfirmar/>
            <Footer/>
        </div>
    );
}
export default ProductosAdmin;