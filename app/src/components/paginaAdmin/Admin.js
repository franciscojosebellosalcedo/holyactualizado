import Footer from "../paginaInicio/Foooter";
import NavBarAdmin from "./NavBarAdmin";

function Admin() {
    return(
        <div className="container__clientes__admin">
            <NavBarAdmin/>
            <div className="contenedor__clientes">
                <div className="contenedor_clientes__titulo" >
                        <h1 className="cliente_titulo ">Administradores</h1>
                        <button className="cliente__btn__agregar">Agregar</button>
                        <input type="text" placeholder="Buscar " className="input input_buscar" />
                </div>  

                <div className="container__tabla__clientes">
                    <table className="tabla__admin__clientes">
                    <thead>
                        <tr>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Fecha Nacimiento</th>
                            <th>Telefono</th>
                            <th>Direccion</th>
                            <th>Sexo</th>
                            <th>Acciones</th>
                        </tr>
					</thead>
                     <tbody>
                            <tr className="clientes__admin__table">
                               <td>Aura</td>
                               <td>Baza Puello</td>
                               <td>25/03/1990</td>
                               <td>3106125920</td>
                               <td>el pueblito</td>
                               <td>femenino</td>
                            <td className="td__btn">
                            <div className="container__btn">
                                <button className="btn__producto_admin btn__producto_admin__editar">Editar</button>
                                <button className="btn__producto_admin btn__producto_admin__eliminar">Eliminar</button>
                                </div>
                            </td>
                            </tr>
                            <tr className="clientes__admin__table">
                               <td>Aura</td>
                               <td>Baza Puello</td>
                               <td>25/03/1990</td>
                               <td>3106125920</td>
                               <td>el pueblito</td>
                               <td>femenino</td>
                            <td className="td__btn">
                            <div className="container__btn">
                                <button className="btn__producto_admin btn__producto_admin__editar">Editar</button>
                                <button className="btn__producto_admin btn__producto_admin__eliminar">Eliminar</button>
                                </div>
                            </td>
                            </tr>
                            <tr className="clientes__admin__table">
                               <td>Aura</td>
                               <td>Baza Puello</td>
                               <td>25/03/1990</td>
                               <td>3106125920</td>
                               <td>el pueblito</td>
                               <td>femenino</td>
                            <td className="td__btn">
                            <div className="container__btn">
                                <button className="btn__producto_admin btn__producto_admin__editar">Editar</button>
                                <button className="btn__producto_admin btn__producto_admin__eliminar">Eliminar</button>
                                </div>
                            </td>
                            </tr>
                            <tr className="clientes__admin__table">
                               <td>Aura</td>
                               <td>Baza Puello</td>
                               <td>25/03/1990</td>
                               <td>3106125920</td>
                               <td>el pueblito</td>
                               <td>femenino</td>
                            <td className="td__btn">
                            <div className="container__btn">
                                <button className="btn__producto_admin btn__producto_admin__editar">Editar</button>
                                <button className="btn__producto_admin btn__producto_admin__eliminar">Eliminar</button>
                                </div>
                            </td>
                            </tr>
                           
                        
                    </tbody>

                    </table>
                </div> 
                  
            </div>
            <Footer/>
        </div>
    );
}
export default Admin;