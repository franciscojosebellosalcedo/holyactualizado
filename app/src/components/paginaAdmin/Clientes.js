import Footer from "../paginaInicio/Foooter";
import NavBarAdmin from "./NavBarAdmin";
import ModalClienteAdmin from "../modal/ModalAdminCliente";
import React from "react";
import ModalConfirmar from "../modal/ModalConfirmar";

function Clientes(props) {

    function agregarClienteAdmin(e) {
        e.preventDefault();
        
        const agregar_cliente=document.querySelector(".perfil_cliente");
        agregar_cliente.classList.add("mostrar__modal__editar__perfil");

    }


    function eliminarClienteoAdmin(e) {
        e.preventDefault();
        const eliminar_cliente=document.querySelector(".cont_modal_confirmar");
        eliminar_cliente.classList.add("mostrar_modal_confirmar");

    }
    
    
    
    return (
        <div className="container__clientes__admin">
            <NavBarAdmin />
            <div className="contenedor__clientes">
                <div className="contenedor_clientes__titulo" >
                    <h1 className="cliente_titulo ">Clientes</h1>
                    <button className="cliente__btn__agregar" onClick={(e)=>agregarClienteAdmin(e)}>Agregar</button>
                    <input type="text" placeholder="Buscar " className="input input_buscar" />
                </div>

                <div className="container__tabla__clientes">
                    <table className="tabla__admin__clientes">
                        <thead>
                            <tr>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>Fecha Nacimiento</th>
                                <th>Documento</th>
                                <th>Telefono</th>
                                <th>Direccion</th>
                                <th>Sexo</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                props.clientes.map((cliente) => (
                                    <tr key={cliente.documento} className="clientes__admin__table">
                                        <td>{cliente.nombres}</td>
                                        <td>{cliente.apellidos}</td>
                                        <td>{cliente.fecha_nacimiento}</td>
                                        <td>{cliente.documento}</td>
                                        <td>{cliente.telefono}</td>
                                        <td>{cliente.direccion}</td>
                                        <td>{cliente.sexo}</td>
                                        <td className="td__btn">
                                            <div className="container__btn">
                                                 <button className="btn__producto_admin btn__producto_admin__editar">Editar</button>
                                                <button className="btn__producto_admin btn__producto_admin__eliminar" onClick={(e)=>eliminarClienteoAdmin(e)}>Eliminar</button>
                                            </div>
                                        </td>
                                    </tr>
                                ))
                            }



                        </tbody>

                    </table>
                </div>

            </div>
           <ModalClienteAdmin/>
           <ModalConfirmar/>
            <Footer />
        </div>
    );
}
export default Clientes;