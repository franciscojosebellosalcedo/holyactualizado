import React from "react";
import { NavLink, Link } from "react-router-dom";
import ModalEditarPerfil from "../paginaCliente/ModalEditarPerfil";
function NavBarAdmin() {
    function mostrarModalEditarPerfil(e) {
        e.preventDefault();
        const modalPerfil = document.querySelector(".container__editar__perfil");
        modalPerfil.classList.add("mostrar__modal__editar__perfil");
    }
    return (
        <header className="header">
            <nav className="header-nav">
                <Link to="/admin/inicio"> <img className="header-icon" src={require("../../icon/holyIcon.jpeg")} alt="icon" /></Link>
                <h2 className="header-titulo">El Pan De Cada Día</h2>

                <input type="checkbox" id="check" />
                <label for="check" className="menu">
                    <div className="linea linea-arriba"></div>
                    <div className="linea linea-medio"></div>
                    <div className="linea linea-abajo"></div>
                </label>

                <ul className="header-lista">
                    <li className="header-item"><NavLink to="/admin/productos" className="btn__mostrar__compra">Productos</NavLink></li>
                    <li className="header-item"><NavLink to="/admin/clientes">Clientes</NavLink></li>
                    <li className="header-item"><NavLink to="/admin/admins">Admins</NavLink></li>
                    <li className="header-item"><NavLink to="/admin/gestion-contable">Gestion contable</NavLink></li>
                    <div className="container__icon">
                        <div className="icono__perfil">
                            <ul className="nav_vertical">
                                <li onClick={(e) => mostrarModalEditarPerfil(e)}><Link to={"#"} className="item__perfil">Perfil</Link></li>
                                <li><Link to={"#"}>Cerrar sesion</Link></li>
                            </ul>
                        </div>
                    </div>
                </ul>
            </nav>
            <ModalEditarPerfil />
        </header>
    );
}

export default NavBarAdmin;