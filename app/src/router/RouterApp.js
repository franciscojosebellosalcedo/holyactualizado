//importamos la libreria react-router-dom para hacer uso de sus componente
import { BrowserRouter, Routes, Route } from "react-router-dom";
import PaginaInicio from "../pages/PaginaInicio";
//importamos los componentes  paginas que tendra la app
import React, { useEffect, useState } from "react";
import PaginaNoEncontrada from "../pages/PaginaNoEncontrada";
import PaginaNosotros from "../pages/PaginaNosotros";
import PaginaProductos from "../pages/PaginaProductos";

import ModalLogin from "../components/modal/ModalLogin";
import ModalRegistro from "../components/modal/ModalRegistro";
import PaginaCiente from "../pages/PageCliente";
import MisCompras from "../components/paginaCliente/MisCompras";
import PaginaAdmin from "../pages/PaginaAdmin";
import ProductosAdmin from "../components/paginaAdmin/ProductosAdmin";
import Clientes from "../components/paginaAdmin/Clientes.js";
import Admin from "../components/paginaAdmin/Admin.js";
import ModalMensaje from "../components/modal/ModalMensaje";

//creamos el componente de function RouterApp para renderizar las rutas al navegador
function RouterApp() {
  const [mensaje, setMensaje] = useState("ttttt");
  const [productos, setProductos] = useState([
    {
      codigo: "001",
      nombre: "Cortao",
      descripcion:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae impedit alias iure quis commodi labore.",
      precio: "2000",
    },
    {
      codigo: "001",
      nombre: "Tajao",
      descripcion:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae impedit alias iure quis commodi labore.",
      precio: "1000",
    },
    {
      codigo: "003",
      nombre: "Frances",
      descripcion:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae impedit alias iure quis commodi labore.",
      precio: "1000",
    },
    {
      codigo: "004",
      nombre: "Integral",
      descripcion:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae impedit alias iure quis commodi labore.",
      precio: "1200",
    },
    {
      codigo: "005",
      nombre: "Croazan",
      descripcion:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae impedit alias iure quis commodi labore.",
      precio: "1200",
    },
    {
      codigo: "005",
      nombre: "Arequipe",
      descripcion:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae impedit alias iure quis commodi labore.",
      precio: "1000",
    },
    {
      codigo: "005",
      nombre: "Bocca Queso",
      descripcion:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae impedit alias iure quis commodi labore.",
      precio: "1000",
    },
    {
      codigo: "005",
      nombre: "Mogolla",
      descripcion:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae impedit alias iure quis commodi labore.",
      precio: "1000",
    },
  ]);
  const [clientes, setClientes] = useState([
    {
      nombres: "Francisco",
      apellidos: "Bello Salcedo",
      telefono: "3207566039",
      documento: "1007130073",
      direccion: "San Rafael",
      sexo: "masculino",
      usuario: {
        correo: "fran@gmail.com",
        contrasenia: "francisco",
      },
    },
  ]);
  const [administradores, setAdministradores] = useState([
    {
      nombres: "Francisco",
      apellidos: "Bello Salcedo",
      telefono: "3207566039",
      documento: "1007130073",
      direccion: "San Rafael",
      sexo: "masculino",
      usuario: {
        correo: "fran@gmail.com",
        contrasenia: "francisco",
      },
    },
  ]);
  useEffect(() => {
    let data = localStorage.getItem("clientes");
    if (data) {
      data = JSON.parse(data);
      console.log(data);
      setClientes(data);
    }
  }, []);
  function mostrarModalMensaje() {
    let modalMensaje = document.querySelector(".container-mensaje");
    modalMensaje.classList.add("mostrar-mensaje");
  }

  return (
    //utlizamos el componente BrowserRouter para que  la app sea navegable
    //utlizamos el componente Routes para englobar las rutas de la app
    //utlizamos el componente Route para definir una ruta dentro de la app
    //utlizamos el atributo path en el componente Route para definir el nombre de una ruta dentro de la app
    //creamos la ruta con path="*" en caso de que el usuario quiera buscar otra ruta pero no exista
    //utlizamos el atributo element en el componente Route para retornar la pagina o componente dentro de app

    <div>
      <BrowserRouter>
        <ModalMensaje mensaje={mensaje} />
        <ModalLogin
          clientes={clientes}
          fnModalMensaje={mostrarModalMensaje}
          fnMensaje={setMensaje}
        />
        <ModalRegistro
          clientes={clientes}
          fnSetClientes={setClientes}
          fnModalMensaje={mostrarModalMensaje}
          fnMensaje={setMensaje}
        />
        <Routes>
          <Route path="/" element={<PaginaInicio />}></Route>
          <Route
            path="/productos"
            element={<PaginaProductos productosGenerales={productos} />}
          ></Route>
          <Route path="/nosotros" element={<PaginaNosotros />}></Route>

          <Route
            path="/cliente/inicio"
            element={<PaginaCiente listaProductos={productos} />}
          ></Route>
          <Route path="/cliente/mis_compras" element={<MisCompras />}></Route>
          <Route path="/cliente/perfil" element={<div>Mi Perfil</div>}></Route>

          <Route path="/admin/inicio" element={<PaginaAdmin />}></Route>
          <Route path="/admin/productos" element={<ProductosAdmin />}></Route>
          <Route
            path="/admin/clientes"
            element={<Clientes clientes={clientes} />}
          ></Route>
          <Route path="/admin/admins" element={<Admin />}></Route>
          <Route
            path="/admin/gestion-contable"
            element={<div>Gestion contable</div>}
          ></Route>
          <Route path="*" element={<PaginaNoEncontrada />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

//exportamos el componente para luego importarlo en donde nosotros querramos
export default RouterApp;
